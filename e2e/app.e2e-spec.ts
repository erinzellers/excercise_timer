import { LonghornPage } from './app.po';

describe('longhorn App', () => {
  let page: LonghornPage;

  beforeEach(() => {
    page = new LonghornPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
