import { Subject } from 'rxjs/Subject';
import { Exercise } from './exercise.model';

export class TrainingService {
    exerciseChanged = new Subject<Exercise>();

    private availableExercises: Exercise[]=[
        { id: 'crunches', name: 'Crunches', duration: 30, calories: 10 },
        { id: 'squats', name: 'Squats', duration: 60, calories: 40 },
        { id: 'side-lunges', name: 'Side Lunges', duration: 60, calories: 40 },
        { id: 'squat-jumps', name: 'Squat Jumps', duration: 40, calories: 50 }
    ];

    private runningExercise: Exercise; 
    private exercises: Exercise[] = [];

    getAvailableExercises(){
        return this.availableExercises.slice();
    }

    startExercise(selectedId: string) {
        this.runningExercise = this.availableExercises.find(
          ex => ex.id === selectedId
        );
        console.log(selectedId);
        this.exerciseChanged.next({ ...this.runningExercise });
      }

      completeExercise(){
        this.exercises.push({ 
            ...this.runningExercise, 
            date: new Date(), 
            state: 'completed' 
        });
        this.runningExercise = null;
        this.exerciseChanged.next(null);
      }

      cancelExercise(progress: number){
        this.exercises.push({ 
            ...this.runningExercise,
            duration: this.runningExercise.duration * (progress / 100),
            calories: this.runningExercise.calories * (progress / 100), 
            date: new Date(), 
            state: 'cancelled' 
        });
        this.runningExercise = null;
        this.exerciseChanged.next(null);
      }

    getRunningExercise(){
        return { ...this.runningExercise };
    }

    getCompletedOrCancelledExercises(){
        return this.exercises.slice();
    }
}